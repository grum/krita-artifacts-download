# Requirements & Installation

## Python

Script has been developed and tested with Python 3.11
```
Python 3.11.2 (main, Mar 13 2023, 12:18:29) [GCC 12.2.0] on linux
```

It should work on Python 3.10 and Python 3.12 too; need to be tested.



## Python VENV

To use the script, it's better to work in a Python virtual environment especially because of PIP packages required.


```bash
$ python3 -m venv "MY_PYTHON_VENV"
```

## Packages

The [requests](https://requests.readthedocs.io/en/latest/) package is required to use the script.
This package provide high-level methods to manage HTTPS requests.

```bash
$ pip install requests
```


# Script usage

## Configuration file

Configuration file is optional: default values *normaly* use the right settings, but it can be useful (for testing) to use a dedicated configuration file.

Read comments written in the provided configuration file `kad.ini` to get help about settings options.

By default, script looks for `kad.ini` configuration file in the same directory than script.
If not found, script looks for `kad.ini` configuration file in directory `~/.config`.

If a specific configuration file is provided (option `--config`) it will be used.

## Command line options

Script provides 3 commands
| Command | Description |
| --- | --- |
| **`repo`** | Manage repositories<br>- Display information about current default repository<br>- List/Search repositories |
| **`tags`** | Manage tags<br>- Display information about given tag in current repository<br>- List/Search tags in current repository |
| **`artifacts`** | Manage artifacts download |

Each commands have its own options.
Use `--help` to get command usage:

```bash
$ python3 kad.py {command} --help
```

## Errors

Any error will stop script execution with a message and exit code `-1`.

## Examples

Some usage examples.

### Display current repository

```bash
$ python3 kad.py repo
```

Output:
```text
Krita Artifacts Download v1.0.0 (2024-05-18)
- Gitlab host:               https://invent.kde.org

- Repository
    Id:                      206
    Path:                    graphics/krita
    Owned by:                Graphics
    Created at:              2019-04-18 23:06:43
    Last activity:           2024-05-18 05:51:04
```

### Display last tag for current repository

```bash
$ python3 kad.py tags --tag last
```

Output:
```text
Krita Artifacts Download v1.0.0 (2024-05-18)
- Gitlab host:               https://invent.kde.org

- Repository
    Id:                      206
    Path:                    graphics/krita
    Owned by:                Graphics
    Created at:              2019-04-18 23:06:43
    Last activity:           2024-05-18 05:51:04

- Tag
    Name:                    v5.2.2.1
    Created at:              2024-04-30 14:46:40
    Message:                 A temporary release for Android (startup fix)
    Commit:                  ef922fb43ff414b436f42180bf91a17590d22c93
    . Author:                Dmitry Kazakov <dimula73@gmail.com>
    . Authored at:           2024-04-30 14:15:43
    . Title:                 Update version to 5.2.2.1

```

### Search and display all tags '5.2' for current repository

```bash
$ python3 kad.py tags --filter '5.2.2'
```

Output:
```text
Krita Artifacts Download v1.0.0 (2024-05-18)
- Gitlab host:               https://invent.kde.org

- Repository
    Id:                      206
    Path:                    graphics/krita
    Owned by:                Graphics
    Created at:              2019-04-18 23:06:43
    Last activity:           2024-05-18 05:51:04

- Tags
  Filter on:                 '5.2.2'

  #001
    Name:                    v5.2.2.1
    Created at:              2024-04-30 14:46:40
    Message:                 A temporary release for Android (startup fix)
    Commit:                  ef922fb43ff414b436f42180bf91a17590d22c93
    . Author:                Dmitry Kazakov <dimula73@gmail.com>
    . Authored at:           2024-04-30 14:15:43
    . Title:                 Update version to 5.2.2.1

  #002
    Name:                    krita-5.2.2-store
    Created at:              2023-12-07 10:21:46
    Message:                 krita-5.2.2-store
    Commit:                  876c90940caf4221feeb1ca9fb1c79610cef02a6
    . Author:                Halla Rempt <halla@valdyas.org>
    . Authored at:           2023-12-07 10:21:34
    . Title:                 Merge branch 'krita/5.2' into work/rempt/5.2-for-macos-store

  #003
    Name:                    v5.2.2
    Created at:              2023-12-06 12:38:39
    Message:                 Krita 5.2.2
    Commit:                  d9ba1af793aefb07c208af292f98aefc9e8ff67a
    . Author:                Dmitry Kazakov <dimula73@gmail.com>
    . Authored at:           2023-12-06 12:38:26
    . Title:                 Update version to 5.2.2
```

### Download in a dedicated directory all 'android' artifacts from last created tag, using a specific configuration file, and display download progress

```bash
$ python3 kad.py artifacts --tag last --config ~/my_kad.ini --target-directory ~/Tmp/kad --progress --filter android
```

Output:
```text
Krita Artifacts Download v1.0.0 (2024-05-18)
- Gitlab host:               https://invent.kde.org

- Repository
    Id:                      625
    Path:                    dkazakov/krita
    Owned by:                Dmitry Kazakov
    Created at:              2019-10-02 21:42:27
    Last activity:           2024-05-16 09:57:43

- Tag
    Name:                    v5.3.0-prealpha1-test-remove-me
    Created at:              2024-05-16 10:11:36
    Message:                 Krita 5.3.0 Prealpha1 Test Release
    Commit:                  3646955b19fadd7453660ade9351c00253d502c8
    . Author:                Dmitry Kazakov <dimula73@gmail.com>
    . Authored at:           2024-05-16 10:10:07
    . Title:                 [revertme] Temporarily disable signing of MacOS packages

- Jobs

    Pipeline/Job:            689577/1824310
    Name:                    android-build-appbundle-release
    Status:                  success
    Stage:                   deploy
    Created at:              2024-05-16 10:11:50
    Started at:              2024-05-18 00:03:55
    Finished at:             2024-05-18 00:08:16
    Artifact available:      Yes
    . Expire at:             2024-05-28 00:08:10
    . File name:             krita-v5-3-0-prealpha1-test-remove-me.zip
    . File size:             577.03MiB
    . Download:              Success (Target: /home/grum/Tmp/kad/android-build-appbundle-release.zip)
    . Extract files:         Success
                             /home/grum/Tmp/kad/krita-5.3.0-prealpha-release.aab

    Pipeline/Job:            689577/1824308
    Name:                    android-build-armeabi-v7a-release
    Status:                  success
    Stage:                   build
    Created at:              2024-05-16 10:11:50
    Started at:              2024-05-16 10:12:28
    Finished at:             2024-05-16 10:25:05
    Artifact available:      Yes
    . Expire at:             2024-05-26 10:25:02
    . File name:             krita-v5-3-0-prealpha1-test-remove-me-v5.3.0-prealpha1-test-remove-me.zip
    . File size:             686.28MiB
    . Download:              Success (Target: /home/grum/Tmp/kad/android-build-armeabi-v7a-release.zip)
    . Extract files:         Success
                             /home/grum/Tmp/kad/krita-armeabi-v7a-5.3.0-prealpha-release-unsigned.apk

    Pipeline/Job:            689577/1824307
    Name:                    android-build-arm64-v8a-release
    Status:                  success
    Stage:                   build
    Created at:              2024-05-16 10:11:50
    Started at:              2024-05-16 10:12:16
    Finished at:             2024-05-16 10:22:25
    Artifact available:      Yes
    . Expire at:             2024-05-26 10:22:21
    . File name:             krita-v5-3-0-prealpha1-test-remove-me-v5.3.0-prealpha1-test-remove-me.zip
    . File size:             801.84MiB
    . Download:              Success (Target: /home/grum/Tmp/kad/android-build-arm64-v8a-release.zip)
    . Extract files:         Success
                             /home/grum/Tmp/kad/krita-arm64-v8a-5.3.0-prealpha-release-unsigned.apk

    Pipeline/Job:            689577/1824306
    Name:                    android-build-x86_64-release
    Status:                  success
    Stage:                   build
    Created at:              2024-05-16 10:11:50
    Started at:              2024-05-16 10:12:15
    Finished at:             2024-05-16 10:23:55
    Artifact available:      Yes
    . Expire at:             2024-05-26 10:23:40
    . File name:             krita-v5-3-0-prealpha1-test-remove-me-v5.3.0-prealpha1-test-remove-me.zip
    . File size:             815.33MiB
    . Download:              Success (Target: /home/grum/Tmp/kad/android-build-x86_64-release.zip)
    . Extract files:         Success
                             /home/grum/Tmp/kad/krita-x86_64-5.3.0-prealpha-release-unsigned.apk

Artifacts downloaded
- Artifacts: 4
  . Skipped: 0
  . Error:   0
```
