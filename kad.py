# -----------------------------------------------------------------------------
# Krita Artifacts Download
# Copyright (C) 2024 - Grum999
#
# This script allows to download releases artifacts from Krita repository
# -----------------------------------------------------------------------------
# SPDX-License-Identifier: GPL-3.0-or-later
#
# https://spdx.org/licenses/GPL-3.0-or-later.html
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Short description about process applied to download artifacts produced from a
# tags
# _____________________________________________________________________________
#
# Gitlab documentation:
#
# - REST API
#   Global usage of REST API
#   https://docs.gitlab.com/ee/api/rest/
#
# - REST API resources
#   Detailed usage per resrouces type
#   https://docs.gitlab.com/ee/api/api_resources.html
#
#
# All REST API need the project ID
# So script provide a function to list/filter project and get ID
# 0) Get Projects
#     (Doc: https://docs.gitlab.com/ee/api/projects.html)
#     /projects?search=krita
#
# The process to retrieve Artifacts from a tag is pretty simple
# 1) Get tags informations
#     (Doc: https://docs.gitlab.com/ee/api/tags.html)
#     /projects/:id/repository/tags/:tag_name
#
#     Example:
#         Project:    625=dkazakov/krita
#         Tag:        v5.3.0-prealpha1-test-remove-me
#         url:        https://invent.kde.org/api/v4/projects/625/repository/tags/v5.3.0-prealpha1-test-remove-me
#
#     Retrieved information needed:
#     - 'id'
#
# 2) Get commit information
#     (Doc: https://docs.gitlab.com/ee/api/commits.html)
#     /projects/:id/repository/commits/:sha
#
#     Example:
#         Project:    625=dkazakov/krita
#         Commit:     3646955b19fadd7453660ade9351c00253d502c8
#         url:        https://invent.kde.org/api/v4/projects/625/repository/commits/3646955b19fadd7453660ade9351c00253d502c8
#
#     Retrieved information needed:
#     - 'last_pipeline/id'
#
# 3) Get jobs informations
#     (Doc: https://docs.gitlab.com/ee/api/jobs.html)
#     /projects/:id/pipelines/:pipeline_id/jobs
#
#     Example:
#         Project:    625=dkazakov/krita
#         Pipeline:   689577
#         url:        https://invent.kde.org/api/v4/projects/625/pipelines/689577/jobs
#
#     Return a list of jobs, for each jobs, retrieved information needed:
#     - 'id'                              (Job unique ID)
#     Additional information taht can be needed:
#     - 'artifacts_file/filename'         (artifact file name)
#     - 'name'                            ("linux-release", "window-release", "...")
#     - 'stage'                           ("build", "deploy")
#     - 'status'                          ("success")
#
# 4) Download artifact
#     (Doc: https://docs.gitlab.com/ee/api/job_artifacts.html)
#     /projects/:id/jobs/:job_id/artifacts/*artifact_path
#
#     Example:
#         Project:    625=dkazakov/krita
#         Job:        1824304
#         url:        https://invent.kde.org/api/v4/projects/625/jobs/1824304/artifacts
#
#     Here, download binary content
#
# -----------------------------------------------------------------------------


# python modules
import os
import re
import sys
import json
import time
import zipfile
import argparse
import requests
import datetime
import configparser
import dateutil

__NAME__ = "Krita Artifacts Download"
__VERSION__ = "1.0.0"
__DATE__ = "2024-05-18"


# enables ansi escape characters in terminal
os.system("")


def bytesSizeToStr(value, unit=None, decimals=2):
    """Convert a size (given in Bytes) to given unit

    Given unit can be:
    - 'auto'
    - 'autobin' (binary Bytes)
    - 'GiB', 'MiB', 'KiB' (binary Bytes)
    - 'GB', 'MB', 'KB', 'B'
    """
    if unit is None:
        unit = 'autobin'

    if not isinstance(unit, str):
        raise Exception('Given `unit` must be a valid <str> value')

    unit = unit.lower()
    if unit not in ['auto', 'autobin', 'gib', 'mib', 'kib', 'gb', 'mb', 'kb', 'b']:
        raise Exception('Given `unit` must be a valid <str> value')

    if not isinstance(decimals, int) or decimals < 0 or decimals > 8:
        raise Exception('Given `decimals` must be a valid <int> between 0 and 8')

    if not (isinstance(value, int) or isinstance(value, float)):
        raise Exception('Given `value` must be a valid <int> or <float>')

    if unit == 'autobin':
        if value >= 1073741824:
            unit = 'gib'
        elif value >= 1048576:
            unit = 'mib'
        elif value >= 1024:
            unit = 'kib'
        else:
            unit = 'b'
    elif unit == 'auto':
        if value >= 1000000000:
            unit = 'gb'
        elif value >= 1000000:
            unit = 'mb'
        elif value >= 1000:
            unit = 'kb'
        else:
            unit = 'b'

    fmt = f'{{0:.{decimals}f}}{{1}}'

    if unit == 'gib':
        return fmt.format(value/1073741824, 'GiB')
    elif unit == 'mib':
        return fmt.format(value/1048576, 'MiB')
    elif unit == 'kib':
        return fmt.format(value/1024, 'KiB')
    elif unit == 'gb':
        return fmt.format(value/1000000000, 'GB')
    elif unit == 'mb':
        return fmt.format(value/1000000, 'MB')
    elif unit == 'kb':
        return fmt.format(value/1000, 'KB')
    else:
        return fmt.format(value, 'B')


def strAlign(data, width):
    """Format data <list(str)> to <str>, each data item will get width applied

    strAlign(['', 'Key:', 'Value'], [5, 15])
    => "     Key:           Value                    "
    """
    returned = []
    for index, item in enumerate(data):
        if index < len(width):
            returned.append(f"{item:{width[index]}}")
        else:
            returned.append(f"{item}")
    return ''.join(returned)


class Console:
    """A small class dedicated to display information to terminal"""

    @staticmethod
    def warning(message):
        """Display a warning message"""
        print("!!! WARNING:")

        if isinstance(message, (list, tuple)):
            message = '\n    '.join(message)

        print(f"    {message}")

    @staticmethod
    def error(message, exitCode=-1):
        """Display an error message"""
        print("!!! ERROR:")

        if isinstance(message, (list, tuple)):
            message = '\n    '.join(message)

        print(f"    {message}")

        if exitCode != 0:
            # only exit if exit code is not 0
            exit(exitCode)

    @staticmethod
    def display(message):
        """Display given message

        Can be a <str> or a <list(str)>
        """
        if isinstance(message, (list, tuple)):
            message = '\n'.join(message)

        print(message)

    @staticmethod
    def progress(text):
        """Display progress information"""
        # memorize cursor position
        sys.stdout.write('\x1b[?1048h')
        # clear line from cursor position
        sys.stdout.write('\x1b[0K')
        # display text
        sys.stdout.write(text)
        # flush content
        sys.stdout.flush()
        # restore cursor position
        sys.stdout.write('\x1b[?1048l')


class DateTime:
    """Small class which provide high-level date/time method"""

    @staticmethod
    def parseDateTime(dt):
        """parse an iso date/time and return a datetime object"""
        return dateutil.parser.isoparse(dt)

    @staticmethod
    def formatDateTime(dt):
        """return a datetime object formatted as Y-M-D H:m:s (local time)"""
        return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(dt.timestamp()))

    @staticmethod
    def local(dt):
        """parse an iso date/time and return it formatted as Y-M-D H:m:s (local time)"""
        if dt is None:
            return ""
        return DateTime.formatDateTime(DateTime.parseDateTime(dt))


class KArtifactsDownload:
    """Main class for Krita Artifacts Download"""
    DEFAULT_CONFIGURATION_FILE_NAME = "kad.ini"
    GITLAB_REST_API = "/api/v4"

    def __init__(self):
        self.__argsMainParser = None
        self.__argsCmdParser = None
        self.__argsCmdSubparser = None
        self.__argsVar = None

        self.__configurationFile = None
        self.__configuration = {"gitlab": {"host": "https://invent.kde.org",    # default gitlab instance
                                           "project_id": 206,                   # main krita project id on invent.kde.org
                                           "token": None,
                                           "result_per_page": None
                                           },
                                "download": {"directory": None,
                                             "chunkSize": 1024 * 64
                                             },
                                "packages": {"directory": None,
                                             "patterns.android": "^_packaging/krita-.*\.(aab|apk)$",
                                             "patterns.linux": "^_packaging/krita-.*\.(appimage|zsync)$",
                                             "patterns.macos": "^_packaging/krita-.*\.dmg$",
                                             "patterns.windows": "^krita-.*\.(zip|exe|msix)$"
                                             }
                                }

        self.__initParseArguments()
        self.__loadConfigFile(self.__argsVar['OPT_CONFIG_FILE'])
        self.__executeCommand()

    def __loadConfigFile(self, fileName):
        """Load configuration file

        If configuration file doesn't exists or can't be read, script is stopped
        Otherwise, self.__configuration is updated

        If `fileName` is None, script will check for a configuration file 'kad.ini':
        - In script directory
        - In ~/.config directory
        """
        if fileName is None:
            # configuration file not defined, check if a file exists in script directory
            fileName = os.path.join(os.path.dirname(__file__), KArtifactsDownload.DEFAULT_CONFIGURATION_FILE_NAME)

            if not os.path.exists(fileName):
                # configuration file not found, check if a file exists in ~/.config directory
                fileName = os.path.join(os.path.expanduser('~'), '.config', KArtifactsDownload.DEFAULT_CONFIGURATION_FILE_NAME)

            if not os.path.exists(fileName):
                # configuration file not found, use default values
                return
        elif not os.path.exists(fileName):
            # configuration file not found: cancel execution
            Console.error(f"Given configuration file not found: {fileName}")

        config = configparser.ConfigParser()

        try:
            config.read(fileName)
        except Exception as e:
            Console.error([f"Given configuration file can't be read: {fileName}",
                           str(e)
                           ])

        try:
            if 'gitlab' in config:
                if 'host' in config['gitlab'] and config['gitlab']['host'].strip() != '':
                    self.__configuration['gitlab']['host'] = config['gitlab']['host'].strip()

                if 'project_id' in config['gitlab'] and config['gitlab']['project_id'].strip() != '':
                    self.__configuration['gitlab']['project_id'] = int(config['gitlab']['project_id'].strip())

                if 'token' in config['gitlab'] and config['gitlab']['token'].strip() != '':
                    self.__configuration['gitlab']['token'] = config['gitlab']['token'].strip()

                if 'result_per_page' in config['gitlab'] and config['gitlab']['result_per_page'].strip() != '':
                    self.__configuration['gitlab']['result_per_page'] = int(config['gitlab']['result_per_page'].strip())

            if 'download' in config:
                if 'directory' in config['download'] and config['download']['directory'].strip() != '':
                    self.__configuration['download']['directory'] = config['download']['directory'].strip()

                if 'chunkSize' in config['download'] and config['download']['chunkSize'].strip() != '':
                    self.__configuration['download']['chunkSize'] = int(config['download']['chunkSize'].strip())

            if 'packages' in config:
                if 'directory' in config['packages'] and config['packages']['directory'].strip() != '':
                    self.__configuration['packages']['directory'] = config['packages']['directory'].strip()

                if 'patterns.android' in config['packages'] and config['packages']['patterns.android'].strip() != '':
                    self.__configuration['packages']['patterns.android'] = config['packages']['patterns.android'].strip()

                if 'patterns.linux' in config['packages'] and config['packages']['patterns.linux'].strip() != '':
                    self.__configuration['packages']['patterns.linux'] = config['packages']['patterns.linux'].strip()

                if 'patterns.macos' in config['packages'] and config['packages']['patterns.macos'].strip() != '':
                    self.__configuration['packages']['patterns.macos'] = config['packages']['patterns.macos'].strip()

                if 'patterns.windows' in config['packages'] and config['packages']['patterns.windows'].strip() != '':
                    self.__configuration['packages']['patterns.windows'] = config['packages']['patterns.windows'].strip()

        except Exception as e:
            Console.error([f"Given configuration file can't be parsed: {fileName}",
                           str(e)
                           ])

    def __initParseArguments(self):
        """Parse global command line arguments"""
        self.__argsMainParser = argparse.ArgumentParser(description=f'{__NAME__} v{__VERSION__}',
                                                        add_help=False,
                                                        formatter_class=argparse.RawTextHelpFormatter
                                                        )

        self.__argsMainParser.add_argument('-v', '--version',
                                           dest='OPT_VERSION',
                                           action='store_true',
                                           required=False,
                                           help='Display script version'
                                           )

        self.__argsMainParser.add_argument('-h', '--help',
                                           dest='OPT_HELP',
                                           action='store_true',
                                           required=False,
                                           help='Display this help message'
                                           )

        self.__argsCmdParser = argparse.ArgumentParser(parents=[self.__argsMainParser],
                                                       add_help=False,
                                                       formatter_class=argparse.RawTextHelpFormatter
                                                       )
        args, unknown = self.__argsCmdParser.parse_known_args()
        self.__argsVar = vars(args)

        self.__displayVersion()
        if 'OPT_VERSION' in self.__argsVar and self.__argsVar['OPT_VERSION']:
            # only want to display version, no need to continue
            exit()

        self.__argsCmdSubparser = self.__argsCmdParser.add_subparsers(dest='commands')

        self.__initParseArgumentsRepo()
        self.__initParseArgumentsTags()
        self.__initParseArgumentsArtifacts()

        self.__argsVar = vars(self.__argsCmdParser.parse_args())

        if 'OPT_CONFIG_FILE' not in self.__argsVar:
            # if no configuration file is defined, set a default one
            self.__argsVar['OPT_CONFIG_FILE'] = None

    def __initParseArgumentsRepo(self):
        """Parse command line arguments for 'repo' command"""
        subparsersParser = self.__argsCmdSubparser.add_parser('repo',
                                                              help="Manage repositories",
                                                              add_help=False,
                                                              description='\n'.join(["Manage repositories",
                                                                                     "",
                                                                                     "By default, properties for the current repository are returned.",
                                                                                     "Use --list option to display of all available repositories."
                                                                                     ]
                                                                                   ),
                                                              formatter_class=argparse.RawTextHelpFormatter
                                                              )

        subparsersParser.add_argument('-h', '--help',
                                      dest='OPT_HELP',
                                      action='store_true',
                                      required=False,
                                      help='Display this help message'
                                      )

        subparsersParser.add_argument('-c', '--config',
                                       dest='OPT_CONFIG_FILE',
                                       metavar='FILE',
                                       action='store',
                                       required=False,
                                       help='Use the defined configuration file instead of the default one'
                                       )

        subparsersParser.add_argument('-l', '--list',
                                      dest='OPT_REPO_LIST',
                                      action='store_true',
                                      required=False,
                                      help='List all available repositories'
                                      )

        subparsersParser.add_argument('-f', '--filter',
                                      dest='OPT_REPO_LIST_FILTER',
                                      metavar='FILTER',
                                      action='store',
                                      required=False,
                                      help='\n'.join(['When used with --list option, filter repositories',
                                                      'Given filter is a regular expression applied on:',
                                                      '- Repository path',
                                                      '- Repository owner name'
                                                      ])
                                      )

        subparsersParser.add_argument('-1', '--one-line',
                                      dest='OPT_REPO_ONELINE',
                                      action='store_true',
                                      required=False,
                                      help='\n'.join(['Return one result per line',
                                                      'Allows to make grep for example'
                                                      ])
                                      )

    def __initParseArgumentsTags(self):
        """Parse command line arguments for 'tags' command"""
        subparsersParser = self.__argsCmdSubparser.add_parser('tags',
                                                              help="Manage tags for current repository",
                                                              add_help=False,
                                                              description='\n'.join(["Manage tags for current repository",
                                                                                     "",
                                                                                     "By default, list all available tags for current repository.",
                                                                                     "Use --tag option to display information for a specific tag."
                                                                                     ]
                                                                                   ),
                                                              formatter_class=argparse.RawTextHelpFormatter
                                                              )
        subparsersParser.add_argument('-h', '--help',
                                      dest='OPT_HELP',
                                      action='store_true',
                                      required=False,
                                      help='Display this help message'
                                      )

        subparsersParser.add_argument('-c', '--config',
                                       dest='OPT_CONFIG_FILE',
                                       metavar='FILE',
                                       action='store',
                                       required=False,
                                       help='Use the defined configuration file instead of the default one'
                                       )

        mutualExclusive = subparsersParser.add_mutually_exclusive_group(required=False)
        mutualExclusive.add_argument('-t', '--tag',
                                     dest='OPT_TAGS_REF',
                                     metavar='TAG',
                                     action='store',
                                     required=False,
                                     help='\n'.join(['Display details for tag',
                                                     'Provided tag must be valid (exists in repository) ; if not, script will exit with error',
                                                     'Use special tag value \'LAST\' to work on the last created tag'
                                                     ])
                                     )

        mutualExclusive.add_argument('-f', '--filter',
                                     dest='OPT_TAGS_LIST_FILTER',
                                     metavar='FILTER',
                                     action='store',
                                     required=False,
                                     help='\n'.join(['When used in list mode, filter tags',
                                                     'Given filter is a regular expression applied on:',
                                                     '- Tag name',
                                                     '- Tag message'
                                                     ])
                                     )

        subparsersParser.add_argument('-1', '--one-line',
                                      dest='OPT_TAGS_ONELINE',
                                      action='store_true',
                                      required=False,
                                      help='\n'.join(['Return one result per line',
                                                      'Allows to make grep for example'
                                                      ])
                                      )

    def __initParseArgumentsArtifacts(self):
        """Parse command line arguments for 'artifacts' command"""
        subparsersParser = self.__argsCmdSubparser.add_parser('artifacts',
                                                              help="Download artifact",
                                                              add_help=False,
                                                              description="Download artifact",
                                                              formatter_class=argparse.RawTextHelpFormatter
                                                              )

        subparsersParser.add_argument('-h', '--help',
                                      dest='OPT_HELP',
                                      action='store_true',
                                      required=False,
                                      help='Display this help message'
                                      )

        subparsersParser.add_argument('-c', '--config',
                                       dest='OPT_CONFIG_FILE',
                                       metavar='FILE',
                                       action='store',
                                       required=False,
                                       help='Use the defined configuration file instead of the default one'
                                       )

        subparsersParser.add_argument('-l', '--list',
                                      dest='OPT_ARTIFACTS_LIST',
                                      action='store_true',
                                      required=False,
                                      help='List available artifact but do not proceed to download'
                                      )

        subparsersParser.add_argument('-f', '--filter',
                                      dest='OPT_ARTIFACTS_FILTER',
                                      metavar='FILTER',
                                      action='store',
                                      required=False,
                                      help='\n'.join(['When used, will filter artifact to download',
                                                      'Given filter is a regular expression applied on:',
                                                      '- Artifact job name',
                                                      '- Artifact file name'
                                                      ])
                                      )

        subparsersParser.add_argument('-s', '--status',
                                      dest='OPT_ARTIFACTS_STATUS',
                                      metavar='STATUS',
                                      action='store',
                                      required=False,
                                      help='\n'.join(['When used, will filter artifact to download according to status',
                                                      'Use a comma \',\' to provide more than one status; valid status are:',
                                                      '- created',
                                                      '- pending',
                                                      '- running',
                                                      '- failed',
                                                      '- success',
                                                      '- canceled',
                                                      '- skipped',
                                                      '- waiting_for_resource'
                                                      ])
                                      )

        subparsersParser.add_argument('-d', '--target-directory',
                                       dest='OPT_ARTIFACTS_DIRECTORY',
                                       metavar='PATH',
                                       action='store',
                                       required=False,
                                       help='\n'.join(['Define a target where downloaded artifacts have to be saved',
                                                       'If not provided, artifact will be saved:',
                                                       '- In directory defined in configuration file',
                                                       '- In current directory if no target directory is defined'
                                                       ])
                                       )

        mutualExclusive = subparsersParser.add_mutually_exclusive_group(required=False)
        mutualExclusive.add_argument('-t', '--tag',
                                     dest='OPT_ARTIFACTS_TAG',
                                     metavar='TAG',
                                     action='store',
                                     required=False,
                                     help='\n'.join(['Define tag for which artifacts have to be downloaded',
                                                     'Provided tag must be valid (exists in repository) ; if not, script will exit with error',
                                                     'Use special tag value \'LAST\' to work on the last created tag'
                                                     ])
                                     )

        mutualExclusive.add_argument('--commit',
                                     dest='OPT_ARTIFACTS_COMMIT',
                                     metavar='HASH',
                                     action='store',
                                     required=False,
                                     help='\n'.join(['Define commit for which artifacts have to be downloaded',
                                                     'Provided tag must be valid (exists in repository) ; if not, script will exit with error'
                                                     ])
                                     )

        subparsersParser.add_argument('-p', '--progress',
                                      dest='OPT_ARTIFACTS_DLPROGRESS',
                                      action='store_true',
                                      required=False,
                                      help='Display progression for artifacts download'
                                      )

        subparsersParser.add_argument('-e', '--error-stop',
                                      dest='OPT_ARTIFACTS_ERRORSTOP',
                                      action='store_true',
                                      required=False,
                                      help='\n'.join(['By default, if an artifact download is in error, script continue to next artifacts',
                                                      'This option allows to stop script with exit if an artifact download is in error'
                                                      ])
                                      )

        subparsersParser.add_argument('-k', '--keep-artifacts',
                                      dest='OPT_ARTIFACTS_KEEP_AFTER_UNZIP',
                                      action='store_true',
                                      required=False,
                                      help='\n'.join(['By default, once artifacts archive is downloaded and content extracted, archive is removed',
                                                      'This option allows to keep downloaded archive',
                                                      '- Note: downloaded artifacts archives are not removed if extraction is in failure'
                                                      ])
                                      )

        subparsersParser.add_argument('-1', '--one-line',
                                      dest='OPT_ARTIFACTS_ONELINE',
                                      action='store_true',
                                      required=False,
                                      help='\n'.join(['Return one result per line',
                                                      'Allows to make grep for example'
                                                      ])
                                      )

    def __httpRestApiRequest(self, api, parameters={}, exitOnError=True, stream=False):
        """Build & execute a request to gitlab REST API"""
        if not isinstance(parameters, dict):
            raise Exception("Given `parameters` must be a <dict>")

        headers = {}
        if self.__configuration['gitlab']['token']:
            headers['PRIVATE-TOKEN'] = self.__configuration['gitlab']['token']

        returned = requests.get(f"{self.__configuration['gitlab']['host']}{KArtifactsDownload.GITLAB_REST_API}/{api}",
                                params=parameters,
                                headers=headers,
                                stream=stream
                                )
        if exitOnError:
            # check if response is valid, if not display error message and exit script
            try:
                returned.raise_for_status()
            except Exception as e:
                Console.error(["An error occured while trying to connect to Gitlab"] + str(e).split('\n'))

        return returned

    def __httpRestApiCall(self, api, parameters={}):
        """Build & execute High level call to gitlab REST API

        The function automatically manage pagination (if any) and ALWAYS return
        a list of dict of data returned from REST API
        """
        if not isinstance(parameters, dict):
            raise Exception("Given `parameters` must be a <dict>")

        # limit the number of items returned
        # value 0 = no limit
        optionMaxItemsReturned = 0
        if '__opt_max_items_returned' in parameters:
            # if optional limit is provided, API call will be stopped and returned results will be limited
            # to the given number of items
            optionMaxItemsReturned = parameters.pop('__opt_max_items_returned')

        if 'per_page' not in parameters:
            # if specific number of items per page is not provided, use the default one from configuration
            parameters['per_page'] = self.__configuration['gitlab']['result_per_page']

        # First call to REST API
        request = self.__httpRestApiRequest(api, parameters)

        nextPage = None
        if 'X-Next-Page' in request.headers:
            # There's more than one page of data to retrieve
            nextPage = request.headers['X-Next-Page']

        returned = []
        while True:
            # process last REST API call results
            requestContent = request.json()
            if isinstance(requestContent, list):
                returned += requestContent
            else:
                # ensure to always return a list
                returned += [requestContent]

            if optionMaxItemsReturned > 0 and len(returned) >= optionMaxItemsReturned:
                # a limit about number of items to return has been defined, no need to continue to retrieve data
                if len(returned) > optionMaxItemsReturned:
                    returned = returned[0:optionMaxItemsReturned]
                break

            if nextPage:
                # there's still data to retrieve...
                parameters['page'] = nextPage
                request = self.__httpRestApiRequest(api, parameters)
                if 'X-Next-Page' in request.headers:
                    nextPage = request.headers['X-Next-Page']
                else:
                    nextPage = None
            else:
                break

        return returned

    def __checkProjectId(self):
        """Check if project Id is defined in configuration file

        Script is stopped with an error if no project Id is defined
        """
        if not self.__configuration['gitlab']['project_id']:
            Console.error(["No project Id is defined in configuration file",
                           "Please check settings and provide project unique identifier"
                           ])

    def __displayVersion(self):
        """Display version"""
        Console.display(f"{__NAME__} v{__VERSION__} ({__DATE__})")

    def __executeCommand(self):
        """Execute script command"""
        if 'commands' in self.__argsVar and self.__argsVar['commands']:
            commandId = self.__argsVar.pop('commands')

            if 'OPT_HELP' in self.__argsVar and self.__argsVar['OPT_HELP']:
                self.__argsCmdSubparser.choices[commandId].print_help()
            elif commandId == 'repo':
                self.__commandRepo()
            elif commandId == 'tags':
                self.__commandTags()
            elif commandId == 'artifacts':
                if not('OPT_ARTIFACTS_TAG' in self.__argsVar and self.__argsVar['OPT_ARTIFACTS_TAG'] or
                       'OPT_ARTIFACTS_COMMIT' in self.__argsVar and self.__argsVar['OPT_ARTIFACTS_COMMIT']):
                    Console.display("For command 'artifacts', one option --tag or --commit is required")
                    exit(-1)

                self.__commandArtifacts()
        else:
            self.__argsCmdParser.print_help()

    def __getDataRepo(self, simple=True):
        """Execute REST API call for 'repo' command

        Each returned item is a dict, build from JSON data returned by REST API.
        If `simple` parameter is True, then only simplified list of properties is returned.

        Check documentation to get a full list of project properties:
            https://docs.gitlab.com/ee/api/projects.html
        """
        filterRegEx = None
        returned = []

        asSimple = 'true'
        if not simple:
            asSimple = 'false'

        if 'OPT_REPO_LIST' in self.__argsVar and self.__argsVar['OPT_REPO_LIST']:
            # return list of repositories
            if 'OPT_REPO_LIST_FILTER' in self.__argsVar and self.__argsVar['OPT_REPO_LIST_FILTER']:
                # apply filter
                try:
                    filterRegEx = re.compile(self.__argsVar['OPT_REPO_LIST_FILTER'], re.IGNORECASE)
                except Exception as e:
                    Console.error(f"Given filter '{self.__argsVar['OPT_REPO_LIST_FILTER']}' is not a valid regular expression")

            projects = self.__httpRestApiCall('projects',
                                              {'search': 'krita',
                                               'simple': asSimple,
                                               'order_by': 'id',
                                               'sort': 'asc'
                                               }
                                              )
        else:
            # return current defined repository only
            self.__checkProjectId()
            projects = self.__httpRestApiCall(f"projects/{self.__configuration['gitlab']['project_id']}",
                                              {'simple': asSimple
                                               }
                                              )

        for project in projects:
            if project['path'] == 'krita':
                # need to filter on path even after search
                if filterRegEx is None or \
                   filterRegEx.search(project['path_with_namespace']) or \
                   filterRegEx.search(project['namespace']['name']):
                    returned.append(project)

        return returned

    def __getDataTags(self):
        """Execute REST API call for 'tags' command

        Each returned item is a dict, build from JSON data returned by REST API.

        Check documentation to get a full list of tags properties:
            https://docs.gitlab.com/ee/api/tags.html
        """
        filterRegEx = None
        returned = []

        self.__checkProjectId()

        if 'OPT_TAGS_REF' in self.__argsVar and self.__argsVar['OPT_TAGS_REF'] and self.__argsVar['OPT_TAGS_REF'].lower() == 'last':
            # get last tag created for repository
            tags = self.__httpRestApiCall(f"projects/{self.__configuration['gitlab']['project_id']}/repository/tags",
                                          {"order_by": 'updated',
                                           "sort": 'desc',
                                           "__opt_max_items_returned": 1
                                          }
                                         )
        elif 'OPT_TAGS_REF' in self.__argsVar and self.__argsVar['OPT_TAGS_REF']:
            # return defined tag only
            tags = self.__httpRestApiCall(f"projects/{self.__configuration['gitlab']['project_id']}/repository/tags/{self.__argsVar['OPT_TAGS_REF']}")
        else:
            # return list of tags
            if 'OPT_TAGS_LIST_FILTER' in self.__argsVar and self.__argsVar['OPT_TAGS_LIST_FILTER']:
                # apply filter
                try:
                    filterRegEx = re.compile(self.__argsVar['OPT_TAGS_LIST_FILTER'], re.IGNORECASE)
                except Exception as e:
                    Console.error(f"Given filter '{self.__argsVar['OPT_TAGS_LIST_FILTER']}' is not a valid regular expression")

            tags = self.__httpRestApiCall(f"projects/{self.__configuration['gitlab']['project_id']}/repository/tags",
                                          {"order_by": 'updated',
                                           "sort": 'desc'
                                          }
                                         )

        for tag in tags:
            if filterRegEx is None or \
               filterRegEx.search(tag['name']) or \
               filterRegEx.search(tag['message']):
                returned.append(tag)

        return returned

    def __getDataArtifacts(self):
        """Execute REST API call for 'artifacts' command

        Returned value is a <dict>:
        {
            'tag': {},          # Tag properties if any (check __getDataTags for properties), otherwise None
            'commit': {},       # Commit properties (check documentation: https://docs.gitlab.com/ee/api/commits.html)
            'jobs': []          # A list of all jobs (check documentation: https://docs.gitlab.com/ee/api/jobs.html)
                                #   Each jobs provide Artifact information
        }

        Note: this method does not proceed to download of artifacts
        """
        filterRegEx = None
        filterStatus = None
        returned = {'tag': None,
                    'commit': None,
                    'jobs': []
                    }

        self.__checkProjectId()

        if 'OPT_ARTIFACTS_FILTER' in self.__argsVar and self.__argsVar['OPT_ARTIFACTS_FILTER']:
            # apply filter
            try:
                filterRegEx = re.compile(self.__argsVar['OPT_ARTIFACTS_FILTER'], re.IGNORECASE)
            except Exception as e:
                Console.error(f"Given filter '{self.__argsVar['OPT_ARTIFACTS_FILTER']}' is not a valid regular expression")

        if 'OPT_ARTIFACTS_STATUS' in self.__argsVar and self.__argsVar['OPT_ARTIFACTS_STATUS']:
            # apply filter
            filterStatus = self.__argsVar['OPT_ARTIFACTS_STATUS'].split(',')

        tagId = None

        if 'OPT_ARTIFACTS_TAG' in self.__argsVar and self.__argsVar['OPT_ARTIFACTS_TAG']:
            # if tag reference provided, search commit hash for tag
            tagId = self.__argsVar['OPT_ARTIFACTS_TAG']
            self.__argsVar['OPT_TAGS_REF'] = tagId
            returned['tag'] = self.__getDataTags()[0]

            self.__argsVar['OPT_ARTIFACTS_COMMIT'] = returned['tag']['commit']['id']

        # get Commit information ==> need to know last pipeline Id
        returned['commit'] = self.__httpRestApiCall(f"projects/{self.__configuration['gitlab']['project_id']}/repository/commits/{self.__argsVar['OPT_ARTIFACTS_COMMIT']}")[0]

        if 'last_pipeline' not in returned['commit'] or \
           not returned['commit']['last_pipeline'] or \
           'id' not in returned['commit']['last_pipeline']:
            if tagId:
                Console.error(f"There's no pipeline for commit {returned['commit']['id']} (Tag {tagId})")
            else:
                Console.error(f"There's no pipeline for commit {returned['commit']['id']}")

        # get Jobs information for pipeline ==> need to know last pipeline Id
        jobs = self.__httpRestApiCall(f"projects/{self.__configuration['gitlab']['project_id']}/pipelines/{returned['commit']['last_pipeline']['id']}/jobs")

        for job in jobs:
            artifactFile = ''
            if 'artifacts_file' in job and job['artifacts_file']:
                artifactFile = job['artifacts_file']['filename']

            if filterRegEx is not None and not (filterRegEx.search(job['name']) or filterRegEx.search(artifactFile)):
                # does not match filter
                continue

            if filterStatus is not None and job['status'] not in filterStatus:
                # does not match filter
                continue

            returned['jobs'].append(job)

        return returned

    def __downloadArtifact(self, job, progress):
        """Download artifact from job

        Return a tuple:
        - (False, 'error message)')
        - (True, 'filename')
        """
        if 'OPT_ARTIFACTS_DIRECTORY' in self.__argsVar and self.__argsVar['OPT_ARTIFACTS_DIRECTORY']:
            # target directory provided as parameter, use it
            targetDir = self.__argsVar['OPT_ARTIFACTS_DIRECTORY']
        elif self.__configuration['download']['directory']:
            # target directory exists in configuration, use it
            targetDir = self.__configuration['download']['directory']
        else:
            # target directory not defined, use script directory
            targetDir = os.path.dirname(__file__)

        targetDir = os.path.realpath(os.path.expanduser(targetDir))

        try:
            os.makedirs(targetDir, exist_ok=True)
        except Exception as e:
            return (False, f"Can't create output directory: {targetDir}")

        # where to save artifact
        basename, extension = os.path.splitext(job['artifacts_file']['filename'])
        targetFile = os.path.join(targetDir, f"{job['name']}{extension}")

        # stream download
        downloadArtifact = self.__httpRestApiRequest(f"projects/{self.__configuration['gitlab']['project_id']}/jobs/{job['id']}/artifacts", exitOnError=False, stream=True)

        try:
            downloadArtifact.raise_for_status()
        except Exception as e:
            return (False, f"Download in failure: {e}")

        downloadArtifactTotalSize = int(downloadArtifact.headers.get('content-length', 0))
        downloadArtifactCurrentSize = 0
        downloadArtifactLastSize = 0

        nfoDownloadTotalSize = bytesSizeToStr(downloadArtifactTotalSize)
        nfoDownloadCurrentSize = ''
        nfoDownloadCurrentPct = ''
        nfoDownloadCurrentSpeed = ''

        try:
            with open(targetFile, 'wb') as fHandle:
                ts = time.time()
                for data in downloadArtifact.iter_content(chunk_size=self.__configuration['download']['chunkSize']):
                    downloadArtifactCurrentSize += fHandle.write(data)

                    if progress:
                        if time.time() - ts >= 1:
                            deltaTs = time.time() - ts
                            nfoDownloadCurrentSpeed = f" -- {bytesSizeToStr((downloadArtifactCurrentSize - downloadArtifactLastSize)/deltaTs)}/s"

                            ts = time.time()
                            downloadArtifactLastSize = downloadArtifactCurrentSize

                        nfoDownloadCurrentSize = bytesSizeToStr(downloadArtifactCurrentSize)
                        nfoDownloadCurrentPct = f"{100 * downloadArtifactCurrentSize/downloadArtifactTotalSize:.2f}%"

                        Console.progress(f"    > Downloading artifact for '{job['name']}': {nfoDownloadCurrentPct} ({nfoDownloadCurrentSize}/{nfoDownloadTotalSize}{nfoDownloadCurrentSpeed})")

            if progress:
                Console.progress("")

        except Exception as e:
            if progress:
                Console.progress("")
            return (False, f"Download in failure: {e}")

        return (True, targetFile)

    def __extractArtifactPackages(self, zipFileName, progress):
        """Extract packages from artifact

        Return a tuple:
        - (False, 'error message)')
        - (True, ['filename1', ...])
        """
        if osTarget := re.search("^(?P<name>android|linux|macos|windows)-.*\.zip$", os.path.basename(zipFileName)):
            rePatterns = self.__configuration['packages'][f'patterns.{osTarget.group("name")}']
        else:
            return (False, f"Can't determinate OS target for file: {targetDir}")

        if 'OPT_ARTIFACTS_DIRECTORY' in self.__argsVar and self.__argsVar['OPT_ARTIFACTS_DIRECTORY']:
            # target directory provided as parameter, use it
            targetDir = self.__argsVar['OPT_ARTIFACTS_DIRECTORY']
        elif self.__configuration['download']['directory']:
            # target directory exists in configuration, use it
            targetDir = self.__configuration['packages']['directory']
        else:
            # target directory not defined, use script directory
            targetDir = os.path.dirname(__file__)

        targetDir = os.path.realpath(os.path.expanduser(targetDir))

        try:
            os.makedirs(targetDir, exist_ok=True)
        except Exception as e:
            return (False, f"Can't create output directory: {targetDir}")


        try:
            returnedFiles = []
            with zipfile.ZipFile(zipFileName, 'r') as zipFile:
                toExtract = []
                for fileNfo in zipFile.infolist():
                    if fileNfo.is_dir():
                        # ignore directories
                        continue

                    if re.search(rePatterns, fileNfo.filename, flags=re.IGNORECASE):
                        # file path/name match pattern, need to extract file
                        targetFileName = os.path.join(targetDir, os.path.basename(fileNfo.filename))
                        returnedFiles.append(targetFileName)

                        if progress:
                            Console.progress(f"    > Extract file: {os.path.basename(fileNfo.filename)}")

                        fileNfo.filename = os.path.basename(fileNfo.filename)
                        zipFile.extract(fileNfo, targetDir)

        except Exception as e:
            if progress:
                Console.progress("")
            return (False, f"Unable to extract file: {e}")

        return (True, returnedFiles)

    def __commandRepo(self):
        """Display informations for repositories

        According to options

        - Information for current repository is defined from configuration file
            If no repository is defined, exit script with an error
            If command can't retrieve data from gitlab, exit script with an error
        - List of available repositories
        """
        if 'OPT_REPO_ONELINE' in self.__argsVar and self.__argsVar['OPT_REPO_ONELINE']:
            projects = self.__getDataRepo()

            for index, project in enumerate(projects, 1):
                Console.display(';'.join([f"#{index}",
                                          f"{project['id']}",
                                          project['path_with_namespace'],
                                          project['namespace']['name'],
                                          DateTime.local(project['created_at']),
                                          DateTime.local(project['last_activity_at'])
                                          ])
                                )
        else:
            displayIndex = False

            Console.display(strAlign(['-', 'Gitlab host:', self.__configuration['gitlab']['host']], (2, 27)))

            if 'OPT_REPO_LIST' in self.__argsVar and self.__argsVar['OPT_REPO_LIST']:
                # display list of repositories
                displayIndex = True
                Console.display(["", "- Repositories"])

                if 'OPT_REPO_LIST_FILTER' in self.__argsVar and self.__argsVar['OPT_REPO_LIST_FILTER']:
                    # apply filter
                    Console.display(strAlign(['', 'Filter on:', f"'{self.__argsVar['OPT_REPO_LIST_FILTER']}'"], (2, 27)))
            else:
                # display current defined repository
                Console.display(["", "- Repository"])

            projects = self.__getDataRepo()

            for index, project in enumerate(projects, 1):
                if displayIndex:
                    Console.display(["",
                                    f"  #{index:03}",
                                    strAlign(['', 'Id:', project['id']], (4, 25))
                                    ])
                else:
                    Console.display(strAlign(['', 'Id:', project['id']], (4, 25)))

                Console.display([strAlign(['', 'Path:', project['path_with_namespace']], (4, 25)),
                                 strAlign(['', 'Owned by:', project['namespace']['name']], (4, 25)),
                                 strAlign(['', 'Created at:', DateTime.local(project['created_at'])], (4, 25)),
                                 strAlign(['', 'Last activity:', DateTime.local(project['last_activity_at'])], (4, 25))
                                 ])

    def __commandTags(self):
        """Display informations for tags

        According to options
        - Return information for given tag
        - List of available tags for repository

        Information for current repository is defined from configuration file
            If no repository is defined, exit script with an error
        """
        displayIndex = False

        if 'OPT_TAGS_ONELINE' in self.__argsVar and self.__argsVar['OPT_TAGS_ONELINE']:
            tags = self.__getDataTags()

            for index, tag in enumerate(tags, 1):
                Console.display(';'.join([f"#{index}",
                                          tag['name'],
                                          DateTime.local(tag['created_at']),
                                          tag['message'],
                                          tag['commit']['id'],
                                          tag['commit']['author_name'],
                                          tag['commit']['author_email'],
                                          DateTime.local(tag['commit']['authored_date']),
                                          tag['commit']['title']
                                          ])
                                )
        else:
            # display current repository informations
            self.__commandRepo()

            if 'OPT_TAGS_REF' in self.__argsVar and self.__argsVar['OPT_TAGS_REF']:
                Console.display(['', "- Tag"])
            else:
                # display list of tags
                displayIndex = True
                Console.display(['', "- Tags"])

                if 'OPT_TAGS_LIST_FILTER' in self.__argsVar and self.__argsVar['OPT_TAGS_LIST_FILTER']:
                    # apply filter
                    Console.display(strAlign(['', 'Filter on:', f"'{self.__argsVar['OPT_TAGS_LIST_FILTER']}'"], (2, 27)))

            tags = self.__getDataTags()

            for index, tag in enumerate(tags, 1):
                if displayIndex:
                    Console.display(["",
                                    f"  #{index:03}",
                                    strAlign(['', 'Name:', tag['name']], (4, 25))
                                    ])
                else:
                    Console.display(strAlign(['', 'Name:', tag['name']], (4, 25)))

                Console.display([strAlign(['', 'Created at:', DateTime.local(tag['created_at'])], (4, 25)),
                                 strAlign(['', 'Message:', tag['message']], (4, 25)),
                                 strAlign(['', 'Commit:', tag['commit']['id']], (4, 25)),
                                 strAlign(['', '. Author:', f"{tag['commit']['author_name']} <{tag['commit']['author_email']}>"], (4, 25)),
                                 strAlign(['', '. Authored at:', DateTime.local(tag['commit']['authored_date'])], (4, 25)),
                                 strAlign(['', '. Title:', tag['commit']['title']], (4, 25))
                                 ])

    def __commandArtifacts(self):
        """Manage artifacts"""
        progress = False
        downloadAllSuccess = True
        downloadNext = True
        extractAllSuccess = True

        nbArtifacts = 0
        nbArtifactsSkipped = 0
        nbArtifactsError = 0
        nbArtifactsESkipped = 0
        nbArtifactsEError = 0

        if 'OPT_ARTIFACTS_DLPROGRESS' in self.__argsVar and self.__argsVar['OPT_ARTIFACTS_DLPROGRESS']:
            progress = True

        if 'OPT_ARTIFACTS_ONELINE' in self.__argsVar and self.__argsVar['OPT_ARTIFACTS_ONELINE']:
            artifacts = self.__getDataArtifacts()

            tagName = ''
            if artifacts['tag']:
                tagName = artifacts['tag']['name']

            nbArtifacts = len(artifacts['jobs'])

            for job in artifacts['jobs']:
                artifactAvailable = 'false'
                if 'artifacts_file' in job and job['artifacts_file']:
                    if DateTime.local(job['artifacts_expire_at']) > DateTime.formatDateTime(datetime.datetime.now()):
                        artifactAvailable = 'true'
                    artifactFile = job['artifacts_file']['filename']
                    artifactSize = str(job['artifacts_file']['size'])
                else:
                    artifactFile = ''
                    artifactSize = ''

                artifactNfo = [tagName,
                               artifacts['commit']['id'],
                               str(job['id']),
                               job['name'],
                               job['status'],
                               job['stage'],
                               DateTime.local(job['created_at']),
                               DateTime.local(job['started_at']),
                               DateTime.local(job['finished_at']),
                               DateTime.local(job['artifacts_expire_at']),
                               artifactAvailable,
                               artifactFile,
                               artifactSize
                               ]

                if not downloadNext or 'OPT_ARTIFACTS_LIST' in self.__argsVar and self.__argsVar['OPT_ARTIFACTS_LIST']:
                    # do not proceed to download if:
                    # - flag 'Download next artifact' is False
                    # - flag 'list only' is True
                    downloadStatus = 'skipped'
                    downloadFileName = ''
                    nbArtifactsSkipped += 1
                    extractStatus = 'skipped'
                    nbArtifactsESkipped += 1
                else:
                    downloadResult = self.__downloadArtifact(job, progress)
                    if downloadResult[0]:
                        downloadStatus = 'success'
                        downloadFileName = downloadResult[1]
                        # download OK, now extract content from archive
                        extractResult = self.__extractArtifactPackages(downloadFileName, progress)

                        if extractResult[0]:
                            extractStatus = 'success'
                            if 'OPT_ARTIFACTS_KEEP_AFTER_UNZIP' not in self.__argsVar or not self.__argsVar['OPT_ARTIFACTS_KEEP_AFTER_UNZIP']:
                                # remove downloaded archive
                                try:
                                    os.remove(downloadFileName)
                                except Exception as e:
                                    pass
                        else:
                            extractStatus = 'failure'
                            extractAllSuccess = False
                            nbArtifactsEError += 1
                    else:
                        downloadStatus = 'failure'
                        downloadFileName = ''
                        downloadAllSuccess = False
                        nbArtifactsError += 1
                        extractStatus = 'skipped'
                        nbArtifactsESkipped += 1

                        if 'OPT_ARTIFACTS_ERRORSTOP' in self.__argsVar and self.__argsVar['OPT_ARTIFACTS_ERRORSTOP']:
                            # option set to stop to download artifacts if one is in failure
                            downloadNext = False

                artifactNfo += [downloadStatus, downloadFileName, extractStatus]
                Console.display(';'.join(artifactNfo))
        else:
            if 'OPT_ARTIFACTS_TAG' in self.__argsVar and self.__argsVar['OPT_ARTIFACTS_TAG']:
                # display current repository & tag informations
                self.__argsVar['OPT_TAGS_REF'] = self.__argsVar['OPT_ARTIFACTS_TAG']
                self.__commandTags()
            else:
                # display current repository informations
                self.__commandRepo()

            Console.display(['', "- Jobs"])

            artifacts = self.__getDataArtifacts()

            nbArtifacts = len(artifacts['jobs'])

            for job in artifacts['jobs']:
                artifactAvailable = 'No'
                if 'artifacts_file' in job and job['artifacts_file']:
                    if DateTime.local(job['artifacts_expire_at']) > DateTime.formatDateTime(datetime.datetime.now()):
                        artifactAvailable = 'Yes'
                    artifactFile = job['artifacts_file']['filename']
                    artifactSize = bytesSizeToStr(job['artifacts_file']['size'])
                else:
                    artifactFile = ''
                    artifactSize = ''

                artifactNfo = ['',
                               strAlign(['', 'Pipeline/Job:', f"{job['pipeline']['id']}/{job['id']}"], (4, 25)),
                               strAlign(['', 'Name:', job['name']], (4, 25)),
                               strAlign(['', 'Status:', job['status']], (4, 25)),
                               strAlign(['', 'Stage:', job['stage']], (4, 25)),
                               strAlign(['', 'Created at:', DateTime.local(job['created_at'])], (4, 25)),
                               strAlign(['', 'Started at:', DateTime.local(job['started_at'])], (4, 25)),
                               strAlign(['', 'Finished at:', DateTime.local(job['finished_at'])], (4, 25)),
                               strAlign(['', 'Artifact available:', artifactAvailable], (4, 25))
                               ]

                if artifactAvailable == 'Yes':
                    artifactNfo += [strAlign(['', '. Expire at:', DateTime.local(job['artifacts_expire_at'])], (4, 25)),
                                    strAlign(['', '. File name:', artifactFile], (4, 25)),
                                    strAlign(['', '. File size:', artifactSize], (4, 25))
                                    ]
                elif 'artifacts_expire_at' in job and job['artifacts_expire_at']:
                    artifactNfo.append(strAlign(['', '. Expired at:', DateTime.local(job['artifacts_expire_at'])], (4, 25)))

                Console.display(artifactNfo)

                if not downloadNext or artifactAvailable == 'No' or 'OPT_ARTIFACTS_LIST' in self.__argsVar and self.__argsVar['OPT_ARTIFACTS_LIST']:
                    # do not proceed to download if:
                    # - flag 'Download next artifact' is False
                    # - flag 'list only' is True
                    downloadStatus = 'Skipped'
                    downloadFileName = ''
                    nbArtifactsSkipped += 1

                    extractStatus = 'Skipped'
                    extractFiles = []
                    nbArtifactsESkipped += 1
                else:
                    downloadResult = self.__downloadArtifact(job, progress)
                    downloadStatus = downloadResult[1]
                    if downloadResult[0]:
                        downloadFileName = downloadResult[1]
                        downloadStatus = f'Success (Target: {downloadFileName})'
                        # download OK, now extract content from archive
                        extractResult = self.__extractArtifactPackages(downloadFileName, progress)

                        if extractResult[0]:
                            extractStatus = 'Success'
                            extractFiles = extractResult[1]

                            if 'OPT_ARTIFACTS_KEEP_AFTER_UNZIP' not in self.__argsVar or not self.__argsVar['OPT_ARTIFACTS_KEEP_AFTER_UNZIP']:
                                # remove downloaded archive
                                try:
                                    os.remove(downloadFileName)
                                except Exception as e:
                                    pass
                        else:
                            extractStatus = f'Failure ({extractResult[1]})'
                            extractFiles = []
                            extractAllSuccess = False
                            nbArtifactsEError += 1
                    else:
                        downloadFileName =  ''
                        downloadStatus = f'Failure ({downloadResult[1]})'
                        downloadAllSuccess = False
                        nbArtifactsError += 1

                        extractStatus = 'Skipped'
                        extractFiles = []
                        nbArtifactsESkipped += 1

                        if 'OPT_ARTIFACTS_ERRORSTOP' in self.__argsVar and self.__argsVar['OPT_ARTIFACTS_ERRORSTOP']:
                            # option set to stop to download artifacts if one is in failure
                            downloadNext = False

                Console.display(strAlign(['', '. Download:', downloadStatus], (4, 25)))
                Console.display(strAlign(['', '. Extract files:', extractStatus], (4, 25)))
                for extractFile in extractFiles:
                    Console.display(strAlign(['', '', extractFile], (4, 25)))

        errors = []

        if downloadAllSuccess:
            if 'OPT_ARTIFACTS_LIST' in self.__argsVar and self.__argsVar['OPT_ARTIFACTS_LIST']:
                Console.display(["",
                                 "Nothing downloaded due to option --list",
                                 f"- Artifacts: {nbArtifacts}",
                                 f"  . Skipped: {nbArtifactsSkipped}",
                                 ])
            else:
                Console.display(["",
                                 "Artifacts downloaded",
                                 f"- Artifacts: {nbArtifacts}",
                                 f"  . Skipped: {nbArtifactsSkipped}",
                                 f"  . Error:   {nbArtifactsError}"
                                 ])
        elif downloadNext:
            Console.display(["",
                             "Some artifacts download were in failure",
                             f"- Artifacts: {nbArtifacts}",
                             f"  . Skipped: {nbArtifactsSkipped}",
                             f"  . Error:   {nbArtifactsError}"
                             ])
        else:
            # in this case, display error + exit code -1
            errors += ["",
                       "Some artifacts download were in failure",
                       f"- Artifacts: {nbArtifacts}",
                       f"  . Skipped: {nbArtifactsSkipped}",
                       f"  . Error:   {nbArtifactsError}"
                       ]

        if not extractAllSuccess:
            errors += ["",
                       "Some downloaded artifacts extraction were in failure",
                       f"- Artifacts: {nbArtifacts}",
                       f"  . Skipped: {nbArtifactsESkipped}",
                       f"  . Error:   {nbArtifactsEError}"
                       ]

        if len(errors):
            Console.error(errors)


if __name__ == '__main__':
    kad = KArtifactsDownload()
